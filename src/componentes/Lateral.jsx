import './css/lateral.css';
import { FaBeer } from 'react-icons/fa';
import { BsFillAlarmFill } from 'react-icons/bs';
import Alarma from './Alarma';

function Lateral(){

    return (
        <div className="lateral">
            <h1>Lateral <BsFillAlarmFill /></h1>
            <FaBeer className="cerveza"/>
            <Alarma mensaje="CERVEZA!!!" gravedad="baja" />
        </div>
    );
}


export default Lateral;


