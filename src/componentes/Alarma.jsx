

import Alert from 'react-bootstrap/Alert';


function Alarma(props) {

    let color = "warning";
    if (props.gravedad === "maxima"){
        color = "danger";
    }


    return (
        <Alert variant={color}>{props.mensaje}</Alert>
    );
}


export default Alarma;