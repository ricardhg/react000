import React from 'react';

import Navegador from './componentes/Navegador';
import Lateral from './componentes/Lateral';
import Contenido from './componentes/Contenido';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';

function App() {
  return (
    <div className="App">
      <Navegador />
      <Container>
        <Row>
          <Col xs="12" md="3">
            <Lateral />
          </Col>
          <Col className="col-12 col-md-9">
            <Contenido />
          </Col>
        </Row>
      </Container>


    </div>
  );
}

export default App;
